FROM golang:1.14-alpine

WORKDIR /go/src/app
COPY *.go /go/src/app/
COPY go.* /go/src/app/

RUN go build -v ./...

CMD ["./trunes"]
