module trunes

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dghubble/go-twitter v0.0.0-20190719072343-39e5462e111f
	github.com/dghubble/oauth1 v0.6.0
)
