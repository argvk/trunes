# Trunes

prunes tweets older than 30 days.

## Why ? 

Ideas constantly change and people evolve in their thought,s twitter is a microverse where people go to flip-out on `*.*`. 

## Getting your auth keys

Head to https://developer.twitter.com/en/apps/ to generate an app

## Running 

```sh
$ env HANDLE=argvk ACCESS_TOKEN=xxxx ACCESS_SECRET=xxxx CONSUMER_KEY=xxxx CONSUMER_SECRET=xxxx make run
```