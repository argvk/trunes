package main

import (
	"log"
	"os"
	"time"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

type authConfig struct {
	ConsumerKey    string
	ConsumerSecret string
	AccessToken    string
	AccessSecret   string
}

func main() {
	handle, found := os.LookupEnv("HANDLE")
	if !found {
		log.Panic("envvar HANDLE not set")
	}
	delThreshold := time.Now().AddDate(0, -1, 0)

	client, err := new(&authConfig{
		AccessToken:    os.Getenv("ACCESS_TOKEN"),
		AccessSecret:   os.Getenv("ACCESS_SECRET"),
		ConsumerKey:    os.Getenv("CONSUMER_KEY"),
		ConsumerSecret: os.Getenv("CONSUMER_SECRET"),
	})
	if err != nil {
		log.Panic("failed to login", err)
	}

	delete(handle, client, delThreshold)
	log.Println("✌️")
}

func delete(handle string, client *twitter.Client, tweetsBefore time.Time) {
	excludeReplies := false
	includeRTs := true

	params := &twitter.UserTimelineParams{
		ScreenName:      handle,
		Count:           200,
		ExcludeReplies:  &excludeReplies,
		IncludeRetweets: &includeRTs,
	}

	var toDelete []twitter.Tweet = []twitter.Tweet{}

	for {
		tweets, _, err := client.Timelines.UserTimeline(params)
		if err != nil {
			log.Println("failed to fetch tweets", err)
		}
		var maxID int64 = -1

		for _, tweet := range tweets {
			createdAt, err := tweet.CreatedAtTime()
			if err != nil {
				log.Println("failed to fetch tweet's createdAt", tweet, err)
			}
			if createdAt.Sub(tweetsBefore).Nanoseconds() < 0 {
				log.Println("tweet createdAt", createdAt)
				toDelete = append(toDelete, tweet)
			}
			maxID = tweet.ID
		}
		if maxID == -1 || maxID == params.MaxID {
			break
		}
		params.MaxID = maxID
	}

	for _, tweet := range toDelete {
		log.Println("deleting", tweet.Text)
		_, _, err := client.Statuses.Destroy(tweet.ID, nil)
		if err != nil {
			log.Println("failed to delete", tweet, err)
		}
	}

}

func new(auth *authConfig) (*twitter.Client, error) {
	config := oauth1.NewConfig(auth.ConsumerKey, auth.ConsumerSecret)
	token := oauth1.NewToken(auth.AccessToken, auth.AccessSecret)
	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)

	return client, nil
}
